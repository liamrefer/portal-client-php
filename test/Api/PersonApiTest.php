<?php
/**
 * PersonApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Portal API
 *
 * API for Portal
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.1
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace Swagger\Client;

use \Swagger\Client\Configuration;
use \Swagger\Client\ApiException;
use \Swagger\Client\ObjectSerializer;

/**
 * PersonApiTest Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class PersonApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for assignUserToPerson
     *
     * Assign a user to a person.
     *
     */
    public function testAssignUserToPerson()
    {
    }

    /**
     * Test case for createPerson
     *
     * Create a person.
     *
     */
    public function testCreatePerson()
    {
    }

    /**
     * Test case for deletePerson
     *
     * Delete a person.
     *
     */
    public function testDeletePerson()
    {
    }

    /**
     * Test case for getALlPersons
     *
     * Find all persons in the system.
     *
     */
    public function testGetALlPersons()
    {
    }

    /**
     * Test case for getPerson
     *
     * Find a specific person.
     *
     */
    public function testGetPerson()
    {
    }

    /**
     * Test case for inviteUserToPerson
     *
     * Invite a user.
     *
     */
    public function testInviteUserToPerson()
    {
    }

    /**
     * Test case for removeUserFromPerson
     *
     * Remove a user from a person.
     *
     */
    public function testRemoveUserFromPerson()
    {
    }

    /**
     * Test case for updatePerson
     *
     * Update a person.
     *
     */
    public function testUpdatePerson()
    {
    }
}
