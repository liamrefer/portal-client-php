<?php
/**
 * AdditionalChargesTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Portal API
 *
 * API for Portal
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.1
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * AdditionalChargesTest Class Doc Comment
 *
 * @category    Class
 * @description AdditionalCharges
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class AdditionalChargesTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "AdditionalCharges"
     */
    public function testAdditionalCharges()
    {
    }

    /**
     * Test attribute "shared_ownership_supplement"
     */
    public function testPropertySharedOwnershipSupplement()
    {
    }

    /**
     * Test attribute "buy_to_let_supplement"
     */
    public function testPropertyBuyToLetSupplement()
    {
    }

    /**
     * Test attribute "leasehold_supplement"
     */
    public function testPropertyLeaseholdSupplement()
    {
    }

    /**
     * Test attribute "transfer_of_equity_fee"
     */
    public function testPropertyTransferOfEquityFee()
    {
    }

    /**
     * Test attribute "lender_supplement"
     */
    public function testPropertyLenderSupplement()
    {
    }

    /**
     * Test attribute "expat_fee"
     */
    public function testPropertyExpatFee()
    {
    }

    /**
     * Test attribute "adult_occupancy_fee"
     */
    public function testPropertyAdultOccupancyFee()
    {
    }

    /**
     * Test attribute "hmo_supplement"
     */
    public function testPropertyHmoSupplement()
    {
    }

    /**
     * Test attribute "buy_to_let_limited_company_supplement"
     */
    public function testPropertyBuyToLetLimitedCompanySupplement()
    {
    }

    /**
     * Test attribute "holiday_let_fee"
     */
    public function testPropertyHolidayLetFee()
    {
    }
}
