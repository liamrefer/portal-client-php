# UserLogin

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **string** | JWT Auth Token | [optional] 
**id** | **string** | Numeric ID of the user | [optional] 
**email** | [**\Swagger\Client\Model\UserPropertiesEmail**](UserPropertiesEmail.md) |  | [optional] 
**remember_token** | **string** | Auth remember token for the user | [optional] 
**created_at** | [**\Swagger\Client\Model\UserPropertiesCreatedAt**](UserPropertiesCreatedAt.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


