# UserExposedFields

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cc_email** | [**\Swagger\Client\Model\UserPropertiesCcEmail**](UserPropertiesCcEmail.md) |  | [optional] 
**created_at** | [**\Swagger\Client\Model\UserPropertiesCreatedAt**](UserPropertiesCreatedAt.md) |  | [optional] 
**direct_dial** | [**\Swagger\Client\Model\UserPropertiesDirectDial**](UserPropertiesDirectDial.md) |  | [optional] 
**email** | [**\Swagger\Client\Model\UserPropertiesEmail**](UserPropertiesEmail.md) |  | [optional] 
**name** | [**\Swagger\Client\Model\UserPropertiesName**](UserPropertiesName.md) |  | [optional] 
**id** | **string** | Numeric ID of the user | [optional] 
**remember_token** | **string** | Auth remember token for the user | [optional] 
**roles** | [**\Swagger\Client\Model\UserPropertiesRoles**](UserPropertiesRoles.md) |  | [optional] 
**team_leader** | [**\Swagger\Client\Model\UserPropertiesTeamLeader**](UserPropertiesTeamLeader.md) |  | [optional] 
**updated_at** | [**\Swagger\Client\Model\UserPropertiesUpdatedAt**](UserPropertiesUpdatedAt.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


