# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cc_email** | **string** | Email address to receive CC emails from the account | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | YYYY-MM-DD H:i:s representation of the time the user was created | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | YYYY-MM-DD H:i:s representation of the time the user was updated | [optional] 
**direct_dial** | **string** | The direct dial phone number for the user | [optional] 
**email** | **string** | The primary email address of the user that they use to login and receive correspondence | [optional] 
**password** | **string** | Password to authenticate the account | [optional] 
**roles** | **string** | A JSON encoded object of the roles the user has | [optional] 
**team_leader** | **string** | The users team leader in their organisation | [optional] 
**name** | **string** | The users real name | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


