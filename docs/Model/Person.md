# Person

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**person_type** | **string** |  | [optional] 
**first_name** | **string** |  | [optional] 
**middle_name** | **string** |  | [optional] 
**last_name** | **string** |  | [optional] 
**previous_names** | **string** |  | [optional] 
**dob** | [**\DateTime**](\DateTime.md) |  | [optional] 
**email** | **string** |  | [optional] 
**phone_mobile** | **string** |  | [optional] 
**phone_home** | **string** |  | [optional] 
**phone_work** | **string** |  | [optional] 
**ni_number** | **string** |  | [optional] 
**correspondence_address_id** | **string** |  | [optional] 
**relationship** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


