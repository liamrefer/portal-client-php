# Body12

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**person_id** | **int** | Add a handler  Add a user as a handler for a remortgage | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


