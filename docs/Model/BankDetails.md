# BankDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_name** | **string** |  | [optional] 
**sort_code** | **string** |  | [optional] 
**account_number** | **string** |  | [optional] 
**bacs_or_chaps** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


