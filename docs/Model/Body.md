# Body

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | [**\Swagger\Client\Model\UserPropertiesEmail**](UserPropertiesEmail.md) | Login  Login the user to retrieve a JWT token | [optional] 
**password** | [**\Swagger\Client\Model\UserPropertiesPassword**](UserPropertiesPassword.md) | Login  Login the user to retrieve a JWT token | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


