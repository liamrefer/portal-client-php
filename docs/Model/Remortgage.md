# Remortgage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reference_number** | **string** |  | [optional] 
**additional_information** | **string** |  | [optional] 
**early_repayment_penalty** | **string** |  | [optional] 
**early_repayment_end_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**postpone_completion** | **bool** |  | [optional] 
**settlement_for_equity_transfer** | **string** |  | [optional] 
**remortgage_address** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


