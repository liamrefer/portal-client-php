# Address

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**country** | [****](.md) |  | [optional] 
**organisation_name** | **string** |  | [optional] 
**administrative_area** | **string** |  | [optional] 
**sub_administrative_area** | **string** |  | [optional] 
**locality** | **string** |  | [optional] 
**dependent_locality** | **string** |  | [optional] 
**postal_code** | **string** |  | [optional] 
**thoroughfare** | **string** |  | [optional] 
**premise** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


