# Body6

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address_id** | **int** | Add an address to a remortgage | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


