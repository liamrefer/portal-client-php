# Lender

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional] 
**address_id** | **string** |  | [optional] 
**allow_address_entry** | **bool** |  | [optional] 
**email** | **string** |  | [optional] 
**fax** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


