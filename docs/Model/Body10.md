# Body10

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**person_id** | **int** | Add an occupier | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


