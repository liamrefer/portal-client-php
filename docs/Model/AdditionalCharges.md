# AdditionalCharges

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shared_ownership_supplement** | **string** |  | [optional] 
**buy_to_let_supplement** | **string** |  | [optional] 
**leasehold_supplement** | **string** |  | [optional] 
**transfer_of_equity_fee** | **string** |  | [optional] 
**lender_supplement** | **string** |  | [optional] 
**expat_fee** | **string** |  | [optional] 
**adult_occupancy_fee** | **string** |  | [optional] 
**hmo_supplement** | **string** |  | [optional] 
**buy_to_let_limited_company_supplement** | **string** |  | [optional] 
**holiday_let_fee** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


