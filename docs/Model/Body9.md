# Body9

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**person_id** | **int** | Add person to persons to be removed  Add a person to the remortgages persons to be removed from deed | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


