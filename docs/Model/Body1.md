# Body1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**\Swagger\Client\Model\UserLoginPropertiesId**](UserLoginPropertiesId.md) | Login as user  Generate an access token for a given user ID, only accessible for users with adequate permission | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


