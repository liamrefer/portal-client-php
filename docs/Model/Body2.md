# Body2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**password** | [**\Swagger\Client\Model\UserPropertiesPassword**](UserPropertiesPassword.md) | Reset a users password  Reset the password of an account with a valid password reset token. | [optional] 
**password_reset_token** | **string** | The password reset token | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


