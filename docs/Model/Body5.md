# Body5

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email_address** | **string** | Invite a user  Given an email address, send an invite to an address for this person letting the user login as the person | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


