# Body3

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | [**\Swagger\Client\Model\UserPropertiesEmail**](UserPropertiesEmail.md) | Begin password reset  Send a password reset email to a user. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


