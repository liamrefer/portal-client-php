# Swagger\Client\RemortgageApi

All URIs are relative to *http://api.portal.intranet/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addAdditionalChargesToRemortgage**](RemortgageApi.md#addAdditionalChargesToRemortgage) | **POST** /remortgage/{id}/additional-charges | Add additional charges
[**addAddressToRemortgage**](RemortgageApi.md#addAddressToRemortgage) | **POST** /remortgage/{id}/address | Add an address to a remortgage
[**addBankDetailsToRemortgage**](RemortgageApi.md#addBankDetailsToRemortgage) | **POST** /remortgage/{id}/bank-details | Add bank details
[**addClient**](RemortgageApi.md#addClient) | **POST** /remortgage/{id}/client | Add a client
[**addExistingMortgageToRemortgage**](RemortgageApi.md#addExistingMortgageToRemortgage) | **POST** /remortgage/{id}/existing-mortgage | Add an existing mortgage to a remortgage
[**addHandler**](RemortgageApi.md#addHandler) | **POST** /remortgage/{id}/handler | Add a handler
[**addNewMortgageToRemortgage**](RemortgageApi.md#addNewMortgageToRemortgage) | **POST** /remortgage/{id}/new-mortgage | Add an new mortgage to a remortgage
[**addOccupier**](RemortgageApi.md#addOccupier) | **POST** /remortgage/{id}/occuper | Add an occupier
[**addPersonToPersonsOnDeed**](RemortgageApi.md#addPersonToPersonsOnDeed) | **POST** /remortgage/{id}/persons-on-deed | Add person to persons on deed
[**addPersonToPersonsToBeAdded**](RemortgageApi.md#addPersonToPersonsToBeAdded) | **POST** /remortgage/{id}/persons-to-be-added | Add person to persons to be added
[**addPersonToPersonsToBeRemoved**](RemortgageApi.md#addPersonToPersonsToBeRemoved) | **POST** /remortgage/{id}/persons-to-be-removed | Add person to persons to be removed
[**countAllRemortgages**](RemortgageApi.md#countAllRemortgages) | **GET** /remortgage/all/count | Get count of remortgages
[**createRemortgage**](RemortgageApi.md#createRemortgage) | **POST** /remortgage | Create a remortgage
[**getAllRemortgages**](RemortgageApi.md#getAllRemortgages) | **GET** /remortgage/all | Get all remortgages in the system
[**getAllRemortgagesForPerson**](RemortgageApi.md#getAllRemortgagesForPerson) | **GET** /remortgage/person/{id} | Find remortgages for a person
[**getMyRemortgages**](RemortgageApi.md#getMyRemortgages) | **GET** /remortgage/mine | Get my remortgages
[**getRemortgage**](RemortgageApi.md#getRemortgage) | **GET** /remortgage/{id} | Get single remortgage
[**markRemortgageAsComplete**](RemortgageApi.md#markRemortgageAsComplete) | **POST** /remortgage/{id}/mark-as-finished | Mark a remortgage as being finished
[**removeAdditionalChargesFromRemortgage**](RemortgageApi.md#removeAdditionalChargesFromRemortgage) | **DELETE** /remortgage/{id}/additional-charges | Remove additional charges
[**removeAddressFromRemortgage**](RemortgageApi.md#removeAddressFromRemortgage) | **DELETE** /remortgage/{id}/address | Remove an address
[**removeBankDetailsFromRemortgage**](RemortgageApi.md#removeBankDetailsFromRemortgage) | **DELETE** /remortgage/{id}/bank-details | Remove bank details
[**removeClient**](RemortgageApi.md#removeClient) | **DELETE** /remortgage/{id}/client/{person-id} | Remove client
[**removeExistingMortgageFromRemortgage**](RemortgageApi.md#removeExistingMortgageFromRemortgage) | **DELETE** /remortgage/{id}/existing-mortgage | Remove an existing mortgage
[**removeHandler**](RemortgageApi.md#removeHandler) | **DELETE** /remortgage/{id}/handler/{person-id} | Remove handler
[**removeNewMortgageFromRemortgage**](RemortgageApi.md#removeNewMortgageFromRemortgage) | **DELETE** /remortgage/{id}/new-mortgage | Remove an new mortgage
[**removeOccupier**](RemortgageApi.md#removeOccupier) | **DELETE** /remortgage/{id}/occupier/{person-id} | Remove occupier
[**removePersonFromPersonsOnDeed**](RemortgageApi.md#removePersonFromPersonsOnDeed) | **DELETE** /remortgage/{id}/persons-on-deed/{person-id} | Remove person from persons on deed
[**removePersonFromPersonsToBeAdded**](RemortgageApi.md#removePersonFromPersonsToBeAdded) | **DELETE** /remortgage/{id}/persons-to-be-added/{person-id} | Remove person from persons to be added
[**removePersonFromPersonsToBeRemoved**](RemortgageApi.md#removePersonFromPersonsToBeRemoved) | **DELETE** /remortgage/{id}/persons-to-be-removed/{person-id} | Remove person from persons to be removed
[**updateMortgageForRemortgage**](RemortgageApi.md#updateMortgageForRemortgage) | **POST** /remortgage/{id}/update-mortgage/{mortgage-id} | Update a new or existing mortgage
[**updateRemortgage**](RemortgageApi.md#updateRemortgage) | **POST** /remortgage/{id} | Update a remortgage


# **addAdditionalChargesToRemortgage**
> addAdditionalChargesToRemortgage($id, $body)

Add additional charges

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of the remortgage to add additional charges to
$body = new \Swagger\Client\Model\AdditionalCharges(); // \Swagger\Client\Model\AdditionalCharges | 

try {
    $apiInstance->addAdditionalChargesToRemortgage($id, $body);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->addAdditionalChargesToRemortgage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of the remortgage to add additional charges to |
 **body** | [**\Swagger\Client\Model\AdditionalCharges**](../Model/AdditionalCharges.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **addAddressToRemortgage**
> addAddressToRemortgage($id, $body)

Add an address to a remortgage

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of the remortgage to add an address to
$body = new \Swagger\Client\Model\Body6(); // \Swagger\Client\Model\Body6 | 

try {
    $apiInstance->addAddressToRemortgage($id, $body);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->addAddressToRemortgage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of the remortgage to add an address to |
 **body** | [**\Swagger\Client\Model\Body6**](../Model/Body6.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **addBankDetailsToRemortgage**
> addBankDetailsToRemortgage($id, $body)

Add bank details

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of the remortgage to bank details to
$body = new \Swagger\Client\Model\BankDetails(); // \Swagger\Client\Model\BankDetails | 

try {
    $apiInstance->addBankDetailsToRemortgage($id, $body);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->addBankDetailsToRemortgage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of the remortgage to bank details to |
 **body** | [**\Swagger\Client\Model\BankDetails**](../Model/BankDetails.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **addClient**
> addClient($id, $body)

Add a client

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of the remortgage to add a client to
$body = new \Swagger\Client\Model\Body11(); // \Swagger\Client\Model\Body11 | 

try {
    $apiInstance->addClient($id, $body);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->addClient: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of the remortgage to add a client to |
 **body** | [**\Swagger\Client\Model\Body11**](../Model/Body11.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **addExistingMortgageToRemortgage**
> addExistingMortgageToRemortgage($id, $body)

Add an existing mortgage to a remortgage

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of the remortgage to add an existing mortgage to
$body = new \Swagger\Client\Model\Remortgage(); // \Swagger\Client\Model\Remortgage | 

try {
    $apiInstance->addExistingMortgageToRemortgage($id, $body);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->addExistingMortgageToRemortgage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of the remortgage to add an existing mortgage to |
 **body** | [**\Swagger\Client\Model\Remortgage**](../Model/Remortgage.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **addHandler**
> addHandler($id, $body)

Add a handler

Add a user as a handler for a remortgage

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of the remortgage to add a handler to
$body = new \Swagger\Client\Model\Body12(); // \Swagger\Client\Model\Body12 | 

try {
    $apiInstance->addHandler($id, $body);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->addHandler: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of the remortgage to add a handler to |
 **body** | [**\Swagger\Client\Model\Body12**](../Model/Body12.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **addNewMortgageToRemortgage**
> addNewMortgageToRemortgage($id, $body)

Add an new mortgage to a remortgage

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of the remortgage to add an new mortgage to
$body = new \Swagger\Client\Model\Remortgage(); // \Swagger\Client\Model\Remortgage | 

try {
    $apiInstance->addNewMortgageToRemortgage($id, $body);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->addNewMortgageToRemortgage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of the remortgage to add an new mortgage to |
 **body** | [**\Swagger\Client\Model\Remortgage**](../Model/Remortgage.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **addOccupier**
> addOccupier($id, $body)

Add an occupier

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of the remortgage to add an occupier to
$body = new \Swagger\Client\Model\Body10(); // \Swagger\Client\Model\Body10 | 

try {
    $apiInstance->addOccupier($id, $body);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->addOccupier: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of the remortgage to add an occupier to |
 **body** | [**\Swagger\Client\Model\Body10**](../Model/Body10.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **addPersonToPersonsOnDeed**
> addPersonToPersonsOnDeed($id, $body)

Add person to persons on deed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of the remortgage to add a persons on deed to
$body = new \Swagger\Client\Model\Body7(); // \Swagger\Client\Model\Body7 | 

try {
    $apiInstance->addPersonToPersonsOnDeed($id, $body);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->addPersonToPersonsOnDeed: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of the remortgage to add a persons on deed to |
 **body** | [**\Swagger\Client\Model\Body7**](../Model/Body7.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **addPersonToPersonsToBeAdded**
> addPersonToPersonsToBeAdded($id, $body)

Add person to persons to be added

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of the remortgage to add a persons to be added to
$body = new \Swagger\Client\Model\Body8(); // \Swagger\Client\Model\Body8 | 

try {
    $apiInstance->addPersonToPersonsToBeAdded($id, $body);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->addPersonToPersonsToBeAdded: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of the remortgage to add a persons to be added to |
 **body** | [**\Swagger\Client\Model\Body8**](../Model/Body8.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **addPersonToPersonsToBeRemoved**
> addPersonToPersonsToBeRemoved($id, $body)

Add person to persons to be removed

Add a person to the remortgages persons to be removed from deed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of the remortgage to add a persons to be removed to
$body = new \Swagger\Client\Model\Body9(); // \Swagger\Client\Model\Body9 | 

try {
    $apiInstance->addPersonToPersonsToBeRemoved($id, $body);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->addPersonToPersonsToBeRemoved: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of the remortgage to add a persons to be removed to |
 **body** | [**\Swagger\Client\Model\Body9**](../Model/Body9.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **countAllRemortgages**
> countAllRemortgages()

Get count of remortgages

JSON count of all remortgages in the system

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->countAllRemortgages();
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->countAllRemortgages: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createRemortgage**
> createRemortgage($body)

Create a remortgage

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\Remortgage(); // \Swagger\Client\Model\Remortgage | 

try {
    $apiInstance->createRemortgage($body);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->createRemortgage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Remortgage**](../Model/Remortgage.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllRemortgages**
> getAllRemortgages()

Get all remortgages in the system

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->getAllRemortgages();
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->getAllRemortgages: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllRemortgagesForPerson**
> getAllRemortgagesForPerson($id)

Find remortgages for a person

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of person to find remortgages for

try {
    $apiInstance->getAllRemortgagesForPerson($id);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->getAllRemortgagesForPerson: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of person to find remortgages for |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getMyRemortgages**
> getMyRemortgages()

Get my remortgages

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->getMyRemortgages();
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->getMyRemortgages: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getRemortgage**
> \Swagger\Client\Model\Remortgage getRemortgage($id)

Get single remortgage

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of remortgage to find

try {
    $result = $apiInstance->getRemortgage($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->getRemortgage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of remortgage to find |

### Return type

[**\Swagger\Client\Model\Remortgage**](../Model/Remortgage.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **markRemortgageAsComplete**
> markRemortgageAsComplete($id)

Mark a remortgage as being finished

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of the remortgage to mark complete

try {
    $apiInstance->markRemortgageAsComplete($id);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->markRemortgageAsComplete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of the remortgage to mark complete |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **removeAdditionalChargesFromRemortgage**
> removeAdditionalChargesFromRemortgage($id)

Remove additional charges

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of the remortgage to remove additional charges from

try {
    $apiInstance->removeAdditionalChargesFromRemortgage($id);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->removeAdditionalChargesFromRemortgage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of the remortgage to remove additional charges from |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **removeAddressFromRemortgage**
> removeAddressFromRemortgage($id)

Remove an address

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of the remortgage to remove an address from

try {
    $apiInstance->removeAddressFromRemortgage($id);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->removeAddressFromRemortgage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of the remortgage to remove an address from |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **removeBankDetailsFromRemortgage**
> removeBankDetailsFromRemortgage($id)

Remove bank details

Remove bank details from a remortgage

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of the remortgage to remove bank details from

try {
    $apiInstance->removeBankDetailsFromRemortgage($id);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->removeBankDetailsFromRemortgage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of the remortgage to remove bank details from |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **removeClient**
> removeClient($id, $person_id)

Remove client

Remove a person from the remortgages clients

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of the remortgage to remove a person from
$person_id = "person_id_example"; // string | ID of the person to remove

try {
    $apiInstance->removeClient($id, $person_id);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->removeClient: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of the remortgage to remove a person from |
 **person_id** | [**string**](../Model/.md)| ID of the person to remove |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **removeExistingMortgageFromRemortgage**
> removeExistingMortgageFromRemortgage($id)

Remove an existing mortgage

Remove details of an existing mortgage from a remortgage

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of the remortgage to remove an existing remortgage from

try {
    $apiInstance->removeExistingMortgageFromRemortgage($id);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->removeExistingMortgageFromRemortgage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of the remortgage to remove an existing remortgage from |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **removeHandler**
> removeHandler($id, $person_id)

Remove handler

Remove a user from the remortgages handlers

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of the remortgage to remove a person from
$person_id = "person_id_example"; // string | ID of the person to remove

try {
    $apiInstance->removeHandler($id, $person_id);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->removeHandler: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of the remortgage to remove a person from |
 **person_id** | [**string**](../Model/.md)| ID of the person to remove |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **removeNewMortgageFromRemortgage**
> removeNewMortgageFromRemortgage($id)

Remove an new mortgage

Remove details of an new mortgage from a remortgage

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of the remortgage to remove an new remortgage from

try {
    $apiInstance->removeNewMortgageFromRemortgage($id);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->removeNewMortgageFromRemortgage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of the remortgage to remove an new remortgage from |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **removeOccupier**
> removeOccupier($id, $person_id)

Remove occupier

Remove a person from the remortgages occupiers

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of the remortgage to remove a person from
$person_id = "person_id_example"; // string | ID of the person to remove

try {
    $apiInstance->removeOccupier($id, $person_id);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->removeOccupier: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of the remortgage to remove a person from |
 **person_id** | [**string**](../Model/.md)| ID of the person to remove |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **removePersonFromPersonsOnDeed**
> removePersonFromPersonsOnDeed($id, $person_id)

Remove person from persons on deed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of the remortgage to remove a person from
$person_id = "person_id_example"; // string | ID of the person to remove

try {
    $apiInstance->removePersonFromPersonsOnDeed($id, $person_id);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->removePersonFromPersonsOnDeed: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of the remortgage to remove a person from |
 **person_id** | [**string**](../Model/.md)| ID of the person to remove |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **removePersonFromPersonsToBeAdded**
> removePersonFromPersonsToBeAdded($id, $person_id)

Remove person from persons to be added

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of the remortgage to remove a person from
$person_id = "person_id_example"; // string | ID of the person to remove

try {
    $apiInstance->removePersonFromPersonsToBeAdded($id, $person_id);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->removePersonFromPersonsToBeAdded: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of the remortgage to remove a person from |
 **person_id** | [**string**](../Model/.md)| ID of the person to remove |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **removePersonFromPersonsToBeRemoved**
> removePersonFromPersonsToBeRemoved($id, $person_id)

Remove person from persons to be removed

Remove a person from the remortgages persons to be removed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of the remortgage to remove a person from
$person_id = "person_id_example"; // string | ID of the person to remove

try {
    $apiInstance->removePersonFromPersonsToBeRemoved($id, $person_id);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->removePersonFromPersonsToBeRemoved: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of the remortgage to remove a person from |
 **person_id** | [**string**](../Model/.md)| ID of the person to remove |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateMortgageForRemortgage**
> updateMortgageForRemortgage($id, $mortgage_id, $body)

Update a new or existing mortgage

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of the remortgage to edit a mortgage on
$mortgage_id = "mortgage_id_example"; // string | ID of the mortgage to update
$body = new \Swagger\Client\Model\Remortgage(); // \Swagger\Client\Model\Remortgage | 

try {
    $apiInstance->updateMortgageForRemortgage($id, $mortgage_id, $body);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->updateMortgageForRemortgage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of the remortgage to edit a mortgage on |
 **mortgage_id** | [**string**](../Model/.md)| ID of the mortgage to update |
 **body** | [**\Swagger\Client\Model\Remortgage**](../Model/Remortgage.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateRemortgage**
> updateRemortgage($id, $body)

Update a remortgage

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemortgageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of remortgage to update
$body = new \Swagger\Client\Model\Remortgage(); // \Swagger\Client\Model\Remortgage | 

try {
    $apiInstance->updateRemortgage($id, $body);
} catch (Exception $e) {
    echo 'Exception when calling RemortgageApi->updateRemortgage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of remortgage to update |
 **body** | [**\Swagger\Client\Model\Remortgage**](../Model/Remortgage.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

