# Swagger\Client\LenderApi

All URIs are relative to *http://api.portal.intranet/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createLender**](LenderApi.md#createLender) | **POST** /lender | Create lender
[**deleteLender**](LenderApi.md#deleteLender) | **DELETE** /lender/{id} | Delete lender
[**getALlLenders**](LenderApi.md#getALlLenders) | **GET** /lender/all | Get lenders
[**getLender**](LenderApi.md#getLender) | **GET** /lender/{id} | Get one lender
[**updateLender**](LenderApi.md#updateLender) | **POST** /lender/{id} | Update lender


# **createLender**
> createLender($body)

Create lender

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\LenderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\Lender(); // \Swagger\Client\Model\Lender | 

try {
    $apiInstance->createLender($body);
} catch (Exception $e) {
    echo 'Exception when calling LenderApi->createLender: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Lender**](../Model/Lender.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteLender**
> deleteLender($id)

Delete lender

Remove a lender from the system /_** Delete an address

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\LenderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of lender to delete

try {
    $apiInstance->deleteLender($id);
} catch (Exception $e) {
    echo 'Exception when calling LenderApi->deleteLender: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of lender to delete |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getALlLenders**
> getALlLenders()

Get lenders

Fetch all lenders in the system

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\LenderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $apiInstance->getALlLenders();
} catch (Exception $e) {
    echo 'Exception when calling LenderApi->getALlLenders: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getLender**
> \Swagger\Client\Model\Lender getLender($id)

Get one lender

Find a lender with a specific ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\LenderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of lender to find

try {
    $result = $apiInstance->getLender($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LenderApi->getLender: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of lender to find |

### Return type

[**\Swagger\Client\Model\Lender**](../Model/Lender.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateLender**
> updateLender($id, $body)

Update lender

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\LenderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of lender to update
$body = new \Swagger\Client\Model\Lender(); // \Swagger\Client\Model\Lender | 

try {
    $apiInstance->updateLender($id, $body);
} catch (Exception $e) {
    echo 'Exception when calling LenderApi->updateLender: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of lender to update |
 **body** | [**\Swagger\Client\Model\Lender**](../Model/Lender.md)|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

