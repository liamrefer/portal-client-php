# Swagger\Client\HistoryApi

All URIs are relative to *http://api.portal.intranet/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getHistory**](HistoryApi.md#getHistory) | **GET** /history/{id} | 


# **getHistory**
> getHistory($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\HistoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = "id_example"; // string | ID of an aggregate to get history for

try {
    $apiInstance->getHistory($id);
} catch (Exception $e) {
    echo 'Exception when calling HistoryApi->getHistory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**string**](../Model/.md)| ID of an aggregate to get history for |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

